param(
    [Parameter(Mandatory = $true)]
    [string]$SubscriptionId,

    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [string]$DirectoryID,

    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [string]$ResourceGroupName,

    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [string]$ResourceGroupLocation,

    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [string]$ServicePrincipalDisplayName,

    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [string]$HomePageServicePrincipal,

    [Parameter(Mandatory = $false)]
    [string]$ServicePrincipalSecretValue,

    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [string]$KeyVaultName,

    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [string]$KeyVaultNameValue,

    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [string]$KeyVaultLocation,

    [Parameter(Mandatory = $true)]
    [ValidateNotNullOrEmpty()]
    [string]$KeyVaultSecretValue
)
#============================ Constants ====================================#
$MODULES_DIRECTORY_NAME = 'Modules'

#=========================== Load module ===================================#
$invocation = (Get-Variable MyInvocation).Value
$rootDirectoryPath = Split-Path $invocation.MyCommand.Path
$moduleDirectoryPath = Join-Path $rootDirectoryPath $MODULES_DIRECTORY_NAME
Get-ChildItem $moduleDirectoryPath -Filter *.psm1 | % { Import-Module -Name (Join-Path $moduleDirectoryPath ($_ -replace '.psm1', [string]::Empty)) -Force }

#=============================== Main ======================================#
$ServicePrincipalSecretValue = Get-RandomCharacters -length 19 -characters 'abcdefghiklmnoprstuvwxyz'
$ServicePrincipalSecretValue += Get-RandomCharacters -length 19 -characters 'ABCDEFGHKLMNOPRSTUVWXYZ'
$ServicePrincipalSecretValue += Get-RandomCharacters -length 4 -characters '1234567890'
$ServicePrincipalSecretValue += Get-RandomCharacters -length 3 -characters '=+=+=+=+=+=+=+=+=+=+=+=+'
$ServicePrincipalSecretValue = Create-ScrambleString $ServicePrincipalSecretValue
Create-AzureConnection -SubscriptionId $SubscriptionId -DirectoryID $DirectoryID
Create-AzureResourseGroup -ResourceGroupName $ResourceGroupName -ResourceGroupLocation $ResourceGroupLocation
$applicationId = Create-AzureActiveDirectoryApplication -ServicePrincipalDisplayName $ServicePrincipalDisplayName -HomePageServicePrincipal $HomePageServicePrincipal -ServicePrincipalSecretValue $ServicePrincipalSecretValue
Create-AzureActiveDirectoryServicePrincipal -servicePrincipal $applicationId -HomePageServicePrincipal $HomePageServicePrincipal -ServicePrincipalDisplayName $ServicePrincipalDisplayName
Create-AzureKeyVault -ResourceGroupName $ResourceGroupName -KeyVaultLocation $KeyVaultLocation -KeyVaultName $KeyVaultName -KeyVaultNameValue $KeyVaultNameValue
Set-AzureKeyVaultSecretValue -KeyVaultSecretValue $KeyVaultSecretValue -KeyVaultName $KeyVaultName -KeyVaultNameValue $KeyVaultNameValue
Get-ListAllParameter -ResourceGroupName $ResourceGroupName -ResourceGroupLocation $ResourceGroupLocation -ServicePrincipalDisplayName $ServicePrincipalDisplayName -HomePageServicePrincipal $HomePageServicePrincipal -ServicePrincipalSecretValue $ServicePrincipalSecretValue -KeyVaultName $KeyVaultName -KeyVaultNameValue $KeyVaultName -KeyVaultLocation $KeyVaultLocation -KeyVaultSecretValue $KeyVaultSecretValue -servicePrincipal $applicationId