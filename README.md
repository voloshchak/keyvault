# README #

This README would normally document whatever steps are needed to get this application up and running.

### What is this repository for? ###

* Create Resource group(or add on existing), Service Principal, Key vault with secret value
* Version 0.1

### How do I get set up? ###
Step 1: Install PowerShellGet

Installing items from the PowerShell Gallery requires the PowerShellGet module. Make sure you have the appropriate version of PowerShellGet and other system requirements. Run the following command to see if you have PowerShellGet installed on your system
```sh
Get-Module PowerShellGet -list | Select-Object Name,Version,Path
```
You should see something similar to the following output:
```sh
Name          Version Path
----          ------- ----
PowerShellGet 1.0.0.1 C:\Program Files\WindowsPowerShell\Modules\PowerShellGet\1.0.0.1\PowerShellGet.psd1
```
If you do not have PowerShellGet installed, see the [How to get PowerShellGet][] section of this article.

[How to get PowerShellGet]: https://docs.microsoft.com/en-us/powershell/azure/install-azurerm-ps?view=azurermps-4.4.1#how-to-get-powershellget

Step 2: Install Azure PowerShell

Installing Azure PowerShell from the PowerShell Gallery requires elevated privileges. Run the following command from an elevated PowerShell session:
```sh
# Install the Azure Resource Manager modules from the PowerShell Gallery
Install-Module AzureRM -AllowClobber
```
By default, the PowerShell gallery is not configured as a Trusted repository for PowerShellGet. The first time you use the PSGallery you see the following prompt:
```sh
Untrusted repository

You are installing the modules from an untrusted repository. If you trust this repository, change
its InstallationPolicy value by running the Set-PSRepository cmdlet.

Are you sure you want to install the modules from 'PSGallery'?
[Y] Yes  [A] Yes to All  [N] No  [L] No to All  [S] Suspend  [?] Help (default is "N"): Y
```
Answer 'Yes' or 'Yes to All' to continue with the installation.
```sn
Note
If you have a version older than 2.8.5.201 of NuGet, you are prompted to download and install the latest version of NuGet.
```
The AzureRM module is a rollup module for the Azure Resource Manager cmdlets. When you install the AzureRM module, any Azure PowerShell module not previously installed is downloaded and from the PowerShell Gallery.
If you have a previous version of Azure PowerShell installed you may receive an error. To resolve this issue, see the [Updating to a new version of Azure PowerShell][] section of this article.

[Updating to a new version of Azure PowerShell]: https://docs.microsoft.com/en-us/powershell/azure/install-azurerm-ps?view=azurermps-4.4.1#update-azps

Step 3: Load the AzureRM module

Once the module is installed, you need to load the module into your PowerShell session. You should do this in a normal (non-elevated) PowerShell session. Modules are loaded using the `Import-Module` cmdlet, as follows:
```sh
Import-Module AzureRM
```

### How to run the script ###
Use `Windows Powershell`, `Windows Powershell ISE`, `Visual Studio Code` etc.
Go to the downloaded folder
```sh
Windows PowerShell
Copyright (C) 2016 Microsoft Corporation. All rights reserved.

PS C:\>cd [*you logic disk*]:/keyvault
```
Run the script
```sh
PS [*you logic disk*]:\keyvault> .\AzurePortalManager.ps1
```

### Explain what info you need to type in script ###
 * SubscriptionId

Go to portal.azure.com --> Subscriptions. Select your Subscriptions. Copy your Subscription ID.

* DirectoryID

Go to portal.azure.com --> Azure Active Directory. Select Properties. Copy Cliend ID.

* ResourceGroupName

Create new or add in existing resource group

* ResourceGroupLocation

List of available regions is: { `centralus, eastasia, southeast asia, eastus, eastus2, westus, westus2, northcentralus, southcentralus, westcentralus, northeurope, westeurope, japaneast, japanwest, brazilsouth, australiasoutheast, australiaeast, westindia, southindia, centralindia, canadacentral, canadaeast, uksouth, ukwest, koreacentral, koreasouth` }

* ServicePrincipalDisplayName

Type service principal name that you want to create

* HomePageServicePrincipal

http://[type_home_page].com

Example: http://example.com
* ServicePrincipalSecretValue

This value is auto generate(random characters):
19 characters on array `'abcdefghiklmnoprstuvwxyz'`

19 characters on array `'ABCDEFGHKLMNOPRSTUVWXYZ'`

4 nubmer on array  `'1234567890'`

3 nubmer on array `'=+=+=+=+=+=+=+=+=+=+=+=+'`

You can change `length` and `characters` in AzurePortalManager.ps1 in section `#===Main===#`

* KeyVaultName

Type key vault name that you want to create

* KeyVaultNameValue

Type key vault name value that you want to create in key vault `(step 8)`

* KeyVaultLocation

List of available regions is: { `centralus, eastasia, southeast asia, eastus, eastus2, westus, westus2, northcentralus, southcentralus, westcentralus, northeurope, westeurope, japaneast, japanwest, brazilsouth, australiasoutheast, australiaeast, westindia, southindia, centralindia, canadacentral, canadaeast, uksouth, ukwest, koreacentral, koreasouth` }

* KeyVaultSecretValue

Your secret value in key vault `(step 9)`

* After executing the script you will see the following information:
```sh
Key Vault secret value
Service Principal name
Key Vault secret name
Resourse group name
Key Vault name
Resourse group location
Service Principal client secret value
Service Primcipal client ID
Service Principal Home page
Key Vault location

```

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact