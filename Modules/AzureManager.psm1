function Create-AzureConnection {
    param(
        [Parameter(Mandatory = $true)]
        [string]$SubscriptionId,

        [Parameter(Mandatory = $true)]
        [string]$DirectoryID
    )

    Write-Log -MessageContent "Login portal Azure"
    Login-AzureRmAccount
    Get-AzureRmSubscription
    Set-AzureRmContext -SubscriptionId $SubscriptionId -TenantId $DirectoryID
}
function Create-AzureResourseGroup {
    param(
        [Parameter(Mandatory = $true)]
        [string]$ResourceGroupName,

        [Parameter(Mandatory = $true)]
        [string]$ResourceGroupLocation
    )

    Write-Log -MessageContent "Start create $ResourceGroupName Resource Group"
    try {
        if ((Get-AzureRmResourceGroup -Name $ResourceGroupName -Location $ResourceGroupLocation) -ne $null)
        {
            Write-Log -MessageContent "$ResourceGroupName was exist"
        }
        else {
            New-AzureRmResourceGroup -Name $ResourceGroupName -Location $ResourceGroupLocation
            Write-Log -MessageContent "$ResourceGroupName Resourse Group was created"
        }
    }
    catch {
        $errorMessage = $_.Exception.Message
        Write-Log "Could not create Resource Group $errorMessage" -IsError
        throw $_
    }
    $checkResourceGroupName = $null
    while ($checkResourceGroupName -eq $null) {
        $checkResourceGroupName = Get-AzureRmResourceGroup -Name $ResourceGroupName -Location $ResourceGroupLocation
    }
}
function Create-AzureActiveDirectoryApplication {
    param(
        [Parameter(Mandatory = $true)]
        [string]$ServicePrincipalSecretValue,

        [Parameter(Mandatory = $true)]
        [string]$ServicePrincipalDisplayName,

        [Parameter(Mandatory = $true)]
        [string]$HomePageServicePrincipal
    )

    Write-Log -MessageContent "Start create Azure Active Directory application"
    try {
        $azureAzureActiveDirectoryApplication = New-AzureRmADApplication -DisplayName $ServicePrincipalDisplayName -HomePage $HomePageServicePrincipal -IdentifierUris $HomePageServicePrincipal -Password $ServicePrincipalSecretValue
        Write-Log -MessageContent "Azure Active Directory application was created"
    }
    catch {
        $errorMessage = $_.Exception.Message
        Write-Log "Could not create Azure Active Directory application $errorMessage" -IsError
        throw $_
    }
    $checkAzureActiveDirectoryApplication = $null
    while ($checkAzureActiveDirectoryApplication -eq $null) {
        $checkAzureActiveDirectoryApplication = Get-AzureRmADApplication -IdentifierUri $HomePageServicePrincipal
    }

    return $azureAzureActiveDirectoryApplication
}
function Create-AzureActiveDirectoryServicePrincipal {
    param(
        [Parameter(Mandatory = $true)]
        [System.Object]$servicePrincipal,

        [Parameter(Mandatory = $true)]
        [string]$HomePageServicePrincipal,

        [Parameter(Mandatory = $true)]
        [string]$ServicePrincipalDisplayName
    )

    Write-Log -MessageContent "Start create Azure Active Directory service principal"
    try {
        New-AzureRmADServicePrincipal -ApplicationId $servicePrincipal.ApplicationId
        Start-Sleep -Seconds 20
        Write-Log -MessageContent "Service principal was creating"
    }
    catch {
        $errorMessage = $_.Exception.Message
        Write-Log "Could not create Azure Active Directory service principal $errorMessage" -IsError
        throw $_
    }
    $checkServicePrincipalName = $null
    while ($checkServicePrincipalName -eq $null) {
        $checkServicePrincipalName = Get-AzureRmADServicePrincipal -ServicePrincipalName $HomePageServicePrincipal
    }
    $checkAzureRmADApplication = $null
    while ($checkAzureRmADApplication -eq $null) {
        $checkAzureRmADApplication = Get-AzureRmADApplication -ApplicationId $checkServicePrincipalName[0].ApplicationId.Guid
    }
    Write-Log -MessageContent "Assignment Contributor role to the service principal"
    try {
        New-AzureRmRoleAssignment -RoleDefinitionName Contributor -ServicePrincipalName $servicePrincipal.ApplicationId
        Write-Log -MessageContent "Role Contributor was assigment"
    }
    catch {
        $errorMessage = $_.Exception.Message
        Write-Log "Could not assignment role to the service principal $errorMessage" -IsError
        throw $_
    }
    $checkAzureActiveDirectorServicePrincipalRoleAssignment = $null
    while ($checkAzureActiveDirectorServicePrincipalRoleAssignment -eq $null) {
        $checkAzureActiveDirectorServicePrincipalRoleAssignment = Get-AzureRmRoleAssignment -RoleDefinitionName Contributor
    }
}
function Create-AzureKeyVault {
    param(
        [Parameter(Mandatory = $true)]
        [string]$ResourceGroupName,

        [Parameter(Mandatory = $true)]
        [string]$KeyVaultLocation,

        [Parameter(Mandatory = $true)]
        [string]$KeyVaultName,

        [Parameter(Mandatory = $true)]
        [string]$KeyVaultNameValue
    )

    Write-Log -MessageContent "Start create $KeyVaultName Key Vault in Location $KeyVaultLocation, with Key Vault secret name $KeyVaultNameValue"
    try {
        New-AzureRmKeyVault -Location $KeyVaultLocation -ResourceGroupName $ResourceGroupName -VaultName $KeyVaultName
        Write-Log -MessageContent "$KeyVaultName Key Vault was created in Location $KeyVaultLocation, with Key Vault secret name $KeyVaultNameValue"
    }
    catch {
        $errorMessage = $_.Exception.Message
        Write-Log "Could not create Azure Key Vault $errorMessage" -IsError
        throw $_
    }
    $checkAzureKeyVault = $null
    while ($checkAzureKeyVault -eq $null) {
        $checkAzureKeyVault = Get-AzureRmKeyVault -VaultName $KeyVaultName
    }
}
function Set-AzureKeyVaultSecretValue {
    param(
        [Parameter(Mandatory = $true)]
        [string]$KeyVaultSecretValue,

        [Parameter(Mandatory = $true)]
        [string]$KeyVaultName,

        [Parameter(Mandatory = $true)]
        [string]$KeyVaultNameValue
    )
    $SecretValue = ConvertTo-SecureString -String "$KeyVaultSecretValue" -AsPlainText -Force
    Write-Log -MessageContent "Start create Key Vault Secret Value"
    try {
        Set-AzureKeyVaultSecret -VaultName $KeyVaultName -Name $KeyVaultNameValue -SecretValue $SecretValue
        Write-Log -MessageContent "Key Vault Secret Value was created"
    }
    catch {
        $errorMessage = $_.Exception.Message
        Write-Log "Could not create Azure Key Vault Secret $errorMessage" -IsError
        throw $_
    }
    $checkAzureKeyVaultSecretValue = $null
    while ($checkAzureKeyVaultSecretValue -eq $null) {
        $checkAzureKeyVaultSecretValue = Get-AzureKeyVaultSecret -Name $KeyVaultNameValue -VaultName $KeyVaultName
    }
}
function Get-ListAllParameter {
    param(
        [Parameter(Mandatory = $true)]
        [string]$ResourceGroupName,

        [Parameter(Mandatory = $true)]
        [string]$ResourceGroupLocation,

        [Parameter(Mandatory = $true)]
        [string]$ServicePrincipalDisplayName,

        [Parameter(Mandatory = $true)]
        [string]$HomePageServicePrincipal,

        [Parameter(Mandatory = $true)]
        [System.Object]$servicePrincipal,

        [Parameter(Mandatory = $true)]
        [string]$ServicePrincipalSecretValue,

        [Parameter(Mandatory = $true)]
        [string]$KeyVaultName,

        [Parameter(Mandatory = $true)]
        [string]$KeyVaultNameValue,

        [Parameter(Mandatory = $true)]
        [string]$KeyVaultLocation,

        [Parameter(Mandatory = $true)]
        [string]$KeyVaultSecretValue
    )
    $showAllValueVariables = @{
        "Resourse group name" = $ResourceGroupName;
        "Resourse group location" = $ResourceGroupLocation;
        "Service Principal name" = $ServicePrincipalDisplayName;
        "Service Principal Home page" = $HomePageServicePrincipal
        "Service Primcipal client ID" = $servicePrincipal.ApplicationId
        "Service Principal client secret value" = $ServicePrincipalSecretValue
        "Key Vault name" = $KeyVaultName
        "Key Vault secret name" = $KeyVaultNameValue
        "Key Vault location" = $KeyVaultLocation
        "Key Vault secret value" = $KeyVaultSecretValue
    }
foreach ($item in $showAllValueVariables) {
    $item
}
}
function Get-RandomCharacters($length, $characters) {
    $random = 1..$length | ForEach-Object { Get-Random -Maximum $characters.length }
    $private:ofs = ""
    return [String]$characters[$random]
}
function Create-ScrambleString([string]$inputString) {
    $characterArray = $inputString.ToCharArray()
    $scrambledStringArray = $characterArray | Get-Random -Count $characterArray.Length
    $outputString = -join $scrambledStringArray
    return $outputString
}

Export-ModuleMember -function 'Create-*'
Export-ModuleMember -function 'Set-*'
Export-ModuleMember -function 'Get-*'
