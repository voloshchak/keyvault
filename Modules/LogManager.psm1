function Write-Log {
    #function returns nothing
    param(
        [Parameter(Mandatory = $true)]
        [string]$MessageContent,

        [Parameter(Mandatory = $false)]
        [switch]$IsError
    )

    $nl = [System.Environment]::NewLine
    if ($IsError) {
        Write-Host "Error:$nl" $MessageContent
    }
    else {
        Write-Host $MessageContent
    }
}

Export-ModuleMember -function 'Write-*'